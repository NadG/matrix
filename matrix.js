var columns = 9;
var rows = 9;
var numMines = 10;
var mine = 'x';
var matrix = [];
var s = 0;
var m = 0;
var flags = 10;

var utilityBox = document.createElement('DIV');
utilityBox.className = "utilityBox";
document.body.appendChild(utilityBox);

var flagBox = document.createElement('DIV');
utilityBox.appendChild(flagBox);
flagBox.className = "flagBox";

var minBox = document.createElement('DIV');
minBox.className = "minBox";
utilityBox.appendChild(minBox);

var secDecBox = document.createElement('DIV');
secDecBox.className = "secDecBox";
utilityBox.appendChild(secDecBox);

//matrice vuota
var generateField = function () {
	for(var x = 0; x < columns; x++) {
	    matrix[x] = [];
	    for(var y = 0; y < rows; y++) {
	        matrix[x][y] = null;
	    }
	}
}
//inserisco le mine nella matrice vuota
var setMine = function () {
	
	var randomCoo = function () {
		var x = Math.floor(Math.random()*columns);
		var y = Math.floor(Math.random()*rows);
		return [x,y];
	}

	for (var i = 0; i < numMines; i++){
		var coo = randomCoo(); //genera coo
		var x = coo[0];
		var y = coo[1];
		while (matrix[x][y] !== mine) {
			matrix[x][y] = mine;
		} 
	}
}
//setto il numero nelle celle in base alle mine presenti nelle vicinanze
var setNumber = function () {
	// 2 - quante bombe ci sono intorno alla cella in esame
	var calculateMinesNumber = function(x, y){
		//3 - ma prima bisogna controllare se la cella in esame è in parte circondata da celle che non esistono, cioè, fuori dalla matrice
		var topL = cellExists(x-1, y-1);//cellexists esegue la relativa funzione in base alle
		var top = cellExists(x, y-1);
		var topR = cellExists(x+1, y-1);

		var right = cellExists(x+1, y);
		var left = cellExists(x-1, y);

		var botL = cellExists(x-1, y+1);
		var bot = cellExists(x, y+1);
		var botR = cellExists(x+1, y+1);

		var cells = [topL,topR, top, right, left, botL, bot, botR];
		var filtered = cells.filter(function(e){ return e === 1});

		return filtered.length;
	}

	// 1 - controllo delle celle libere da mine
	for(var x = 0; x < columns; x++) { 
	    for(var y = 0; y < rows; y++) {
	    	if (matrix[x][y] === null) {
				matrix[x][y] = calculateMinesNumber(x,y);
	    	}	
	    }
	}	
}

var cellExists = function (x, y) {
	if ( (x < 0 || x >= columns) || ( y < 0 || y >= rows) ) {
		return -1;
	} else if ( matrix[x][y] !== mine) {
		return 0;
	} 
	return 1;
}

var createTable = function() {
	var table = document.createElement('DIV');
	document.body.appendChild(table);
	table.className = "table";

	for (var x = 0; x < rows; x++){	
		var row = document.createElement('DIV');

		row.className += "row";	
		table.appendChild(row);

		for (var y = 0; y < columns; y++){
			var cell = document.createElement('DIV');
			row.appendChild(cell);
			cell.className = "cell";

			var span = document.createElement('SPAN');
			span.innerHTML = matrix[x][y];
			cell.appendChild(span);
			(function(){
				var f = x;
				var g = y;
				cell.addEventListener("click", function(event){
					showCell(event, f, g)
				}, false);
			})()
			
			cell.addEventListener("click", startTimer, false);
			cell.addEventListener('contextmenu', showFlag, false);
		}
	}
		
}

var showFlag = function (event) {
    var cell = event.currentTarget;
    event.preventDefault();

    var classes = cell.classList;
    // la cella è già girata?
    if (classes.contains('visible')) {
        return;
    }
    //la cella ha già la bandierina?
    if (!classes.contains("flag")) { //no, posso piazzarne altre?

        if (flags > 0) {//sì, la piazzo e GG
            classes.add("flag");
            --flags;
            flagBox.innerHTML = flags;
        } else {
            //hai finito le flag
        }
    } else {//sì, c'è la bandierina quindi la tolgo
        ++flags;
        classes.remove("flag");

        flagBox.innerHTML = flags;
    }
};



var showCell = function (event,x,y){
    var cell = event.currentTarget;
    var classes = cell.classList;

    if (!classes.contains('visible') && !classes.contains('flag')){
        var value = matrix[x][y];
        cell.classList.toggle('visible');
        if (value === mine) {
            swal({
                title: "GAMEOVER",
                type: "error",
                confirmButtonText: "Restart",
                confirmButtonColor: "#feca01",
            },
            function(){
                setTimeout(function(){
                    location.reload();
                }, 250);
            });
        }

        if (value === 0) {
        	findZeroes(x, y);
        }
    } 
    
};


var getCellNodeFromCoo = function (x,y) {
	console.log(x,y);
	var r = document.getElementsByClassName('row');//riga cliccata
	var rowA = r[x];
	var cells = rowA.children;

	return cells[y];
 	
}

var findZeroes = function (x, y) {

	var topL = isCellNotABomb(x-1, y-1);
	var top = isCellNotABomb(x, y-1);
	var topR = isCellNotABomb(x+1, y-1);

	var right = isCellNotABomb(x+1, y);
	var left = isCellNotABomb(x-1, y);

	var botL = isCellNotABomb(x-1, y+1);
	var bot = isCellNotABomb(x, y+1);
	var botR = isCellNotABomb(x+1, y+1);
 	
 	var cells = [topL,topR, top, right, left, botL, bot, botR];
	var filtered = cells.filter(function(e){ return e !== false});

	//var node = getNodeFromCoo(x,y);
	for ( var i = 0; i < filtered.length; i++) {
		var node = getCellNodeFromCoo(filtered[i][0], filtered[i][1]);
		if(!node.classList.contains('visible') ) {
			node.classList.add('visible');
			if ( matrix[filtered[i][0]][filtered[i][1]] === 0 ) {
				findZeroes(filtered[i][0], filtered[i][1]);
			}
		} 
	}	
}

var isCellNotABomb = function (x, y) {
	if ( (x < 0 || x >= columns) || ( y < 0 || y >= rows) ) {
		return false;
	}  
	 
	if (matrix[x][y] === mine) {
		return false;
	}
	return [x,y];
}


var timer = null;
var startTimer = function() {
	if( timer === null){
		timer = setInterval(function(){
			++s;
		  	if (s >= 60){
				++m;
		    	s = 0;
			}
			secDecBox.innerHTML = s;
			minBox.innerHTML = m +  ' ' + ':';
		},1000);
	} 
}
/*
var print = function(){
	console.log(matrix.map(function(row){
		return row.toString();
	}));
}*/

generateField();
setMine();
setNumber();
createTable();
